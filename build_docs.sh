#!/bin/bash
# Copyright 2020 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -ex
cd "${0%/*}"

if [[ ! -d depot_tools ]]; then
  git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
fi

export PATH="${PWD}/depot_tools:${PATH}"

repo_manifest="https://chromium.googlesource.com/chromiumos/manifest.git"
repo_url="https://chromium.googlesource.com/external/repo.git"

if [[ ! -d repo ]]; then
  mkdir repo
  cd repo
  repo init -u "${repo_manifest}" --repo-url "${repo_url}" -g crosvm
  repo sync
  cd ..
fi


published_url="https://crosvm-ci.gitlab.io/crosvm-ci/"
current_commit=$(git -C repo/src/platform/crosvm rev-parse HEAD)
published_commit=$(curl -sf "${published_url}commit")

if [[ "${published_commit}" = "${current_commit}" ]]; then
  exit
fi

mkdir -p build

builder=crosvm_builder
version="$(cat repo/src/platform/crosvm/ci/image_tag)"

docker run \
  --rm \
  -v "${PWD}"/repo/src:/workspace/src:rw \
  -v "${PWD}"/build:/workspace/scratch:rw \
  "gcr.io/crosvm-packages/${builder}:${version}" \
  "cargo doc --all-features --target-dir /workspace/scratch && chown -R $(id -u) /workspace/scratch"

mv build/doc public

# Redirect the index to the crosvm module doc.
cat >public/index.html <<INDEX
<script>location="crosvm"</script>
INDEX

# Include commit info of this published doc.
echo -n "${current_commit}" >public/commit

